<%-- 
    Document   : login_pretty
    Created on : 2017-07-21, 19:04:23
    Author     : PsLgComp
--%>

<%@page import="java.util.Enumeration"%>
<%@page language="java" contentType="text/html; charset=iso-8859-2"
        pageEncoding="ISO-8859-2"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-2">
        <title>JSP Page</title>
        <!-- Custom styles for this template -->
        <link href="css/login_pretty.css" rel="stylesheet">
    </head>
    <body>
        <%//wy?wietlanie wszystkich parametr�w wej?ciowych z request

            Enumeration<String> params = request.getParameterNames();
            String param;
            while (params.hasMoreElements()) {
                param = params.nextElement();
                out.println("<p>parametr " + param + " = "
                        + request.getParameter(param) + "</p>");
            }
        %>



        <div id="panel">
            <form action ='/JspTests/print' method='post'>
                <label for="username">Nazwa u�ytkownika:</label>
                <input type="text" id="username" name="username">
                <label for="password">Has�o:</label>
                <input type="password" id="password" name="password">
                <div id="lower">
                    <!-- <input type="checkbox"><label class="check" for="checkbox">Zapami?taj mnie!</label> -->
                    <input type="submit" value="Login">
                </div>
            </form>
        </div>
    </body>
</html>
