<%-- 
    Document   : comment
    Created on : 2017-07-21, 20:11:47
    Author     : PsLgComp
--%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>     
        <form action ='<%=request.getContextPath()%>/addpost' method='post'>
            <label for="username">Podaj swój nick:</label>
            <input type="text" id="username" name="nickname">
            <br /><br />
            <label for="posttitle">Wpisz tytuł posta:</label>
            <input type="text" id="posttitle" name="posttitle">
            <br /><br />
            <!-- Pole komentarza (o powiększonych rozmiarach oraz z tekstem domyślnym) -->
            <p>Wpisz treść nowego posta:</p>
            <textarea name="newpost" cols="50" rows="10">Treść nowego posta </textarea>
            <br /><br /><br />
            <input type="submit" value="Wyślij formularz" />
        </form>
    </body>
</html>
