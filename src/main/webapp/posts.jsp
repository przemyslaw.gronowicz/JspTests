<%-- 
    Document   : index
    Created on : 2017-07-21, 12:00:13
    Author     : PsLgComp
--%>

<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
    <jsp:include page="<%="/blog/head.jsp"%>" />
    <body>
        <div class="blog-masthead">
            <jsp:include page="<%="/blog/masthead.jsp"%>" />            
        </div>
        <div class="container">
            <div class="blog-header">                
                <jsp:include page="<%="/blog/header.jsp"%>" />        
            </div>
            <div class="row">
                <div class="col-sm-8 blog-main">
                    <!--jsp:include page="<%="/blog/main.jsp"%>" /-->
                    <jsp:include page="<%="/addpost.jsp"%>" />
                </div><!-- /.blog-main -->
                <div class="col-sm-3 col-sm-offset-1 blog-sidebar">
                    <jsp:include page="<%="/blog/sidebar.jsp"%>" />
                </div><!-- /.blog-sidebar -->
            </div><!-- /.row -->
        </div><!-- /.container -->
        <jsp:include page="<%="/blog/footer.jsp"%>" />
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>');</script>
        <script src="js/bootstrap.min.js"></script>
        <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
        <script src="js/ie10-viewport-bug-workaround.js"></script>
    </body>
</html>


