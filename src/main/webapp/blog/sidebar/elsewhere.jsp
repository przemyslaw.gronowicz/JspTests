<%@taglib prefix="c" uri="http://java.sun.com/jstl/core" %>
<%@ page language="java" pageEncoding="UTF-8"%>
<div class="sidebar-module">
    <h4>Elsewhere</h4>
    <ol class="list-unstyled">
        <li><a href="#">GitHub</a></li>
        <li><a href="#">Twitter</a></li>
        <li><a href="#">Facebook</a></li>
    </ol>
</div>
