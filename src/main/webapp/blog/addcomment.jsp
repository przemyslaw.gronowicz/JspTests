<%-- 
    Document   : comment
    Created on : 2017-07-21, 20:11:47
    Author     : PsLgComp
--%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ page language="java" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>     
        <form action ='/JspTests/print' method='post'>
            <label for="username">Podaj swój nick:</label>
            <input type="text" id="username" name="nickname">
            <!-- Pole komentarza (o powiększonych rozmiarach oraz z tekstem domyślnym) -->
            <p><c:out value='${request.posts[0].title}'/></p>
            <p>Podaj swój komentarz do postu: <c:out value="${posts[pageContext.request.getParameter(post_id)]}"/> </p>
            <textarea name="Komentarz" cols="50" rows="10">Proszę, wpisz tutaj jakiś komentarz...</textarea>
            <br /><br /><br />
            <input type="submit" value="Wyślij formularz" />
        </form>
    </body>
</html>
