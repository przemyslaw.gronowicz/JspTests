<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ page language="java" pageEncoding="UTF-8"%>
<!-- <%//=(request.getRequestURI().endsWith("index.jsp")) ? "active" : ""%>-->
<div class="container">
    <nav class="blog-nav">
        <a class="blog-nav-item 
           <c:if test = "${ fn:endsWith(pageContext.request.requestURI, 'index.jsp') }">active</c:if>" 
               href="index.do">Home</a>
           <a class="blog-nav-item 
           <c:if test = "${ fn:endsWith(pageContext.request.requestURI, 'posts.jsp') }">active</c:if>"
               href="posts.jsp">Posts</a>
           <a class="blog-nav-item 
           <c:if test = "${ fn:endsWith(pageContext.request.requestURI, 'something') }">active</c:if>"
               href="#">Press</a>
           <a class="blog-nav-item 
           <c:if test = "${ fn:endsWith(pageContext.request.requestURI, 'something') }">active</c:if>"
               href="#">New hires</a>
           <a class="blog-nav-item 
           <c:if test = "${ fn:endsWith(pageContext.request.requestURI, 'about.jsp') }">active</c:if>"
           href="about.jsp">About</a>
    </nav>
</div>
